import React, { Component } from 'react';
import { Route, Redirect } from 'react-router-dom';
import { useSelector } from 'react-redux';
import { selectToken } from '../app/components/Login/slice/selectors';

export const GuardedRoute = ({ component: Component, ...rest }) => {
  const token = useSelector(selectToken);

  return (
    <Route
      {...rest}
      render={props =>
        token ? <Component {...props} /> : <Redirect to="/login" />
      }
    />
  );
};
