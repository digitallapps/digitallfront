import { User } from './User';

export interface Client {
  adresse?: string;
  createdBy?: string;
  createdDate?: string;
  description?: string;
  domaineActivite: string;
  email: string;
  fax?: string;
  id?: number;
  inscriptionTraitee?: boolean;
  inscriptionTraiteeLe?: string;
  inscriptionValidee?: boolean;
  inscriptionValideeLe?: string;
  inscriptionValideePar?: string;
  lastModifiedBy?: string;
  lastModifiedDate?: string;
  statut: string;
  telephone1: string;
  telephone2?: string;
  user?: User;
  visibilite: string;
}

export interface ClientPersonneMorale {
  client: Client;
  capital?: number;
  createdBy?: string;
  createdDate?: string;
  denomination?: string;
  id?: number;
  lastModifiedBy?: string;
  lastModifiedDate?: string;
  nombreEmploye?: number;
  sigle?: string;
  typeStructure: string;
}

export interface ClientPersonnePhysique {
  client: Client;
  createdBy?: string;
  createdDate?: string;
  fonction?: string;
  id?: number;
  lastModifiedBy?: string;
  lastModifiedDate?: string;
  nom: string;
  prenom: string;
}

export enum Visibilite {
  PUBLIC = 'PUBLIC',
  PRIVATE = 'PRIVATE',
}

export enum Statut {
  ACTIF = 'ACTIF',
  PENDING = 'PENDING',
  INACTIF = 'INACTIF',
  SUSPENDU = 'SUSPENDU',
  RESILIE = 'RESILIE',
  SUPPRIME = 'SUPPRIME',
  REJETE = 'REJETE',
  PUBLIEE = 'PUBLIEE',
  ACCEPTEE = 'ACCEPTEE',
  CLOTUREE = 'CLOTUREE',
  ARCHIVEE = 'ARCHIVEE',
  ATTRIBUEE = 'ATTRIBUEE',
  TRAITEE = 'TRAITEE',
  SELECTIONNEE = 'SELECTIONNEE',
  VALIDEE = 'VALIDEE',
  INFRUCTUEUSE = 'INFRUCTUEUSE',
  EN_ATTENTE_SUSPENSION = 'EN_ATTENTE_SUSPENSION',
  EN_ATTENTE_PUBLICATION = 'EN_ATTENTE_PUBLICATION',
  EN_ATTENTE_VALIDATION = 'EN_ATTENTE_VALIDATION',
  EN_ATTENTE_LEVEE_SUSPENSION = 'EN_ATTENTE_LEVEE_SUSPENSION',
  EN_ATTENTE_RESILIATION = 'EN_ATTENTE_RESILIATION',
  EN_ATTENTE_RENDRE_INFRUCTUEUSE = 'EN_ATTENTE_RENDRE_INFRUCTUEUSE',
  EN_ATTENTE_REJET = 'EN_ATTENTE_REJET',
  EN_ATTENTE_CLOTURATION = 'EN_ATTENTE_CLOTURATION',
  EN_ATTENTE_ATTRIBUTION = 'EN_ATTENTE_ATTRIBUTION',
  EN_ATTENTE = 'EN_ATTENTE',
  EN_ATTENTE_SELECTION = 'EN_ATTENTE_SELECTION',
}
