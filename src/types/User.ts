export interface User {
  activated: boolean;
  authorities: any;
  email: string;
  firstName: string;
  id: number;
  langKey: string;
  lastModifiedBy: string;
  lastModifiedDate: string;
  lastName: string;
  login: string;
  resetDate: string;
  statut: string;
}
