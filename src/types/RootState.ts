import { ThemeState } from 'styles/theme/slice/types';
import { LoginState } from 'app/components/Login/slice/types';
import { OffresState } from 'app/components/OffresList/slice/types';
import { SignupState } from 'app/components/Signup/slice/types';
import { OffreSingleState } from 'app/components/OffreSingle/slice/types';
import { CreerOffreState } from 'app/components/CreerOffre/slice/types';
import { ModifierOffreState } from 'app/components/ModifierOffre/slice/types';
import { ProfilClientState } from 'app/components/ProfilClient/slice/types';
// [IMPORT NEW CONTAINERSTATE ABOVE] < Needed for generating containers seamlessly

/* 
  Because the redux-injectors injects your reducers asynchronously somewhere in your code
  You have to declare them here manually
  Properties are optional because they are injected when the components are mounted sometime in your application's life. 
  So, not available always
*/
export interface RootState {
  theme?: ThemeState;
  login?: LoginState;
  offres?: OffresState;
  signup?: SignupState;
  offreSingle?: OffreSingleState;
  creerOffre?: CreerOffreState;
  modifierOffre?: ModifierOffreState;
  profilClient?: ProfilClientState;
  // [INSERT NEW REDUCER KEY ABOVE] < Needed for generating containers seamlessly
}
