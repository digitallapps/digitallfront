import * as React from 'react';
import { createRenderer } from 'react-test-renderer/shallow';

import { ProfilClientPage } from '..';

const shallowRenderer = createRenderer();

describe('<HomePage />', () => {
  it('should render and match the snapshot', () => {
    shallowRenderer.render(<ProfilClientPage />);
    const renderedOutput = shallowRenderer.getRenderOutput();
    expect(renderedOutput).toMatchSnapshot();
  });
});
