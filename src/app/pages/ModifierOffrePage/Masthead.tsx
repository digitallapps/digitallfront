import * as React from 'react';
import styled from 'styled-components/macro';
import { Title } from './components/Title';
import { Lead } from './components/Lead';

export function Masthead() {
  return (
    <Wrapper>
      <Title>Offre </Title>
      <Lead>Modification de l'offre!</Lead>
    </Wrapper>
  );
}

const Wrapper = styled.main`
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
`;
