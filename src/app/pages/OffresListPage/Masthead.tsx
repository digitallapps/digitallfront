import * as React from 'react';
import Button from 'react-bootstrap/Button';
import styled from 'styled-components/macro';
import { Title } from './components/Title';
import { Lead } from './components/Lead';

export function Masthead() {
  return (
    <Wrapper>
      <Title>Offres à la Une</Title>
      <Lead>
        Nos offres sont adaptées à vos compétences pour votre satisfaction et
        celle de nos partenaires.
        <BUTTON href="/offres/creer">Nouvelle offre</BUTTON>
      </Lead>
    </Wrapper>
  );
}

const Wrapper = styled.main`
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
`;

const BUTTON = styled(Button)`
  border-radius: 50px;
`;
