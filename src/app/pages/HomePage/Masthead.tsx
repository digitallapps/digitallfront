import * as React from 'react';
import styled from 'styled-components/macro';
import { Logos } from './Logos';
import { Title } from './components/Title';
import { Lead } from './components/Lead';

export function Masthead() {
  return (
    <Wrapper>
      <Logos />
      <Title>Freelancing meets Digital</Title>
      <Lead>
        Now you can use the best platform for freelancers and clients.
      </Lead>
    </Wrapper>
  );
}

const Wrapper = styled.main`
  height: 40vh;
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
  min-height: 320px;
`;
