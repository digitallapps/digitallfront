import * as React from 'react';
import { createRenderer } from 'react-test-renderer/shallow';

import { CreerOffrePage } from '..';

const shallowRenderer = createRenderer();

describe('<HomePage />', () => {
  it('should render and match the snapshot', () => {
    shallowRenderer.render(<CreerOffrePage />);
    const renderedOutput = shallowRenderer.getRenderOutput();
    expect(renderedOutput).toMatchSnapshot();
  });
});
