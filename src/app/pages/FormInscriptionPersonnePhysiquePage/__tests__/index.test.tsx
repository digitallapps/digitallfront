import * as React from 'react';
import { createRenderer } from 'react-test-renderer/shallow';

import { FormInscriptionPersonnePhysiquePage } from '..';

const shallowRenderer = createRenderer();

describe('<LoginPage />', () => {
  it('should render and match the snapshot', () => {
    shallowRenderer.render(<FormInscriptionPersonnePhysiquePage />);
    const renderedOutput = shallowRenderer.getRenderOutput();
    expect(renderedOutput).toMatchSnapshot();
  });
});
