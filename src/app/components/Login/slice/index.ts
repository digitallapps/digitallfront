import { PayloadAction } from '@reduxjs/toolkit';
import { createSlice } from 'utils/@reduxjs/toolkit';
import { useInjectReducer, useInjectSaga } from 'utils/redux-injectors';
import { loginSaga } from './saga';
import { LoginState } from './types';

export const initialState: LoginState = {
  token: '',
  success: false,
  error: '',
  message: '',
  currentUser: {},
};

const slice = createSlice({
  name: 'login',
  initialState,
  reducers: {
    loginRequested(state, action: PayloadAction<any>) {
      state.error = '';
    },
    logoutRequested(state) {
      state.token = '';
      state.message = '';
      state.success = true;
      state.currentUser = {};
    },
    authenticationSucceded(state, action: PayloadAction<any>) {
      state.error = '';
      state.message = '';
      state.success = true;
      state.token = action.payload;
    },
    authenticationFailed(state, action: PayloadAction<string>) {
      console.log(action);
      state.error = action.payload;
      state.message = action.payload;
      state.success = false;
    },
    userRetrieved(state, action: PayloadAction<any>) {
      state.currentUser = action.payload;
      state.success = true;
      state.message = '';
    },
  },
});

export const { actions: loginActions } = slice;

export const useLoginSlice = () => {
  useInjectReducer({ key: slice.name, reducer: slice.reducer });
  useInjectSaga({ key: slice.name, saga: loginSaga });
  return { actions: slice.actions };
};

/**
 * Example Usage:
 *
 * export function MyComponentNeedingThisSlice() {
 *  const { actions } = useLoginSlice();
 *
 *  const onButtonClick = (evt) => {
 *    dispatch(actions.someAction());
 *   };
 * }
 */
