import { createSelector } from '@reduxjs/toolkit';

import { RootState } from 'types';
import { initialState } from '.';

const selectSlice = (state: RootState) => state.login || initialState;

export const selectError = createSelector([selectSlice], state => state.error);
export const selectSuccess = createSelector(
  [selectSlice],
  state => state.success,
);
export const selectMessage = createSelector(
  [selectSlice],
  state => state.message,
);
export const selectToken = createSelector([selectSlice], state => state.token);
export const selectUser = createSelector(
  [selectSlice],
  state => state.currentUser,
);
