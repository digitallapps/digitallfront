import { call, put, takeEvery } from 'redux-saga/effects';
import { loginActions as actions } from '.';

function getUser(login, token) {
  const FETCH_URL = `http://178.62.98.157:8080/digitall/api/users/${login}`;

  return fetch(FETCH_URL, {
    method: 'GET',
    headers: {
      accept: '*/*',
      'Content-Type': 'application/json',
      Authorization: `Bearer ${token}`,
    },
  })
    .then(response => response.json())
    .catch(error => ({ error }));
}

function authenticateAPI(userData) {
  const FETCH_URL = 'http://178.62.98.157:8080/digitall/api/authenticate';

  return fetch(FETCH_URL, {
    method: 'POST',
    headers: {
      accept: '*/*',
      'Content-Type': 'application/json',
    },
    body: JSON.stringify(userData),
  })
    .then(response => response.json())
    .catch(error => ({ error }));
}

function* authenticateWorker(action) {
  const response = yield call(authenticateAPI, action.payload);
  if (response.id_token) {
    yield put(actions.authenticationSucceded(response.id_token));
    const user = yield call(
      getUser,
      action.payload.username,
      response.id_token,
    );
    yield put(actions.userRetrieved(user));
  } else {
    yield put(actions.authenticationFailed(response.detail));
  }
}

export function* loginSaga() {
  yield takeEvery(actions.loginRequested.type, authenticateWorker);
}
