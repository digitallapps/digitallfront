/* --- STATE --- */
export interface LoginState {
  token: string;
  error: string;
  success: boolean;
  message: string;
  currentUser: any;
}
