import React from 'react';
import { Link, Redirect } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';

import { ErrorMessage, Formik } from 'formik';
import * as Yup from 'yup';
import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Card from 'react-bootstrap/Card';
import Form from 'react-bootstrap/Form';
import Button from 'react-bootstrap/Button';
import Alert from 'react-bootstrap/Alert';
import styled from 'styled-components/macro';

import { useLoginSlice } from './slice/index';
import { selectError } from './slice/selectors';
import { selectMessage } from './slice/selectors';
import { selectSuccess } from './slice/selectors';
import { selectToken } from './slice/selectors';

export function Login() {
  const dispatch = useDispatch();
  const { actions } = useLoginSlice();
  const error = useSelector(selectError);
  const message = useSelector(selectError);
  const successLogin = useSelector(selectSuccess);
  const token = useSelector(selectToken);

  if (token) return <Redirect to="/offres" />;

  return (
    <Container>
      <Row>
        <Col lg={{ offset: 1, span: 10 }}>
          {error && (
            <Alert variant="danger" className="text-center">
              {error}
            </Alert>
          )}
          {successLogin && (
            <Alert variant="success" className="text-center">
              {message}
            </Alert>
          )}
          <CARD text="white" primary="true" body>
            <Formik
              initialValues={{ userEmail: '', userPassword: '' }}
              validationSchema={Yup.object({
                userEmail: Yup.string()
                  .email('Adresse e-mail invalide')
                  .required('Adresse e-mail requise'),
                userPassword: Yup.string().required('Mot de passe requis'),
              })}
              onSubmit={(values, formActions) => {
                dispatch(
                  actions.loginRequested({
                    username: values.userEmail,
                    password: values.userPassword,
                    rememberMe: true,
                  }),
                );
                formActions.setSubmitting(false);
              }}
            >
              {formik => (
                <Form onSubmit={formik.handleSubmit}>
                  <Form.Group as={Row}>
                    <Form.Label column="lg" md={3}>
                      Email
                    </Form.Label>
                    <Col md={9} lg={9}>
                      <FormControl
                        id="userEmail"
                        type="email"
                        placeholder="Veuillez saisir votre adresse email"
                        {...formik.getFieldProps('userEmail')}
                      />
                      <span style={{ color: '#F063B8' }}>
                        <ErrorMessage name="userEmail" />
                      </span>
                    </Col>
                  </Form.Group>
                  <Form.Group as={Row}>
                    <Form.Label column="lg" md={3}>
                      Mot de passe
                    </Form.Label>
                    <Col md={9} lg={9}>
                      <FormControl
                        id="userPassword"
                        type="password"
                        placeholder="Veuillez saisir votre mot de passe"
                        {...formik.getFieldProps('userPassword')}
                      />
                      <span style={{ color: '#F063B8' }}>
                        <ErrorMessage name="userPassword" />
                      </span>
                    </Col>
                  </Form.Group>
                  <Row>
                    <Col md={{ offset: 3 }}>
                      <BUTTON type="submit"> Se Connecter </BUTTON>
                    </Col>
                  </Row>
                  <Row>
                    <Col md={{ offset: 3 }}>
                      <StyledLink to="/signup">
                        Vous n'avez pas de compte? Inscrivez-vous en un click!{' '}
                      </StyledLink>
                    </Col>
                  </Row>
                </Form>
              )}
            </Formik>
          </CARD>
        </Col>
      </Row>
    </Container>
  );
}

const BUTTON = styled(Button)`
  border-radius: 50px;
`;

const CARD = styled(Card)`
  border-radius: 50px;
  padding: 25px;
  background: ${props => (props.primary ? '#212353' : 'white')};
`;

const FormControl = styled(Form.Control)`
  border-radius: 25px;
`;

const StyledLink = styled(Link)`
  color: ${p => p.theme.primary};
  text-decoration: none;

  &:hover {
    text-decoration: underline;
    opacity: 0.8;
  }

  &:active {
    opacity: 0.4;
  }
`;
