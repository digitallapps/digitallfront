import { take, call, put, select, takeLatest } from 'redux-saga/effects';
import { signupActions as actions } from '.';

function creerPhysiqueAPI(clientData) {
  console.log('Données envoyées', clientData);
  const FETCH_URL =
    'http://178.62.98.157:8080/digitall/api/clients/personne-physique/inscription';
  return fetch(FETCH_URL, {
    method: 'POST',
    headers: {
      accept: '*/*',
      'Content-Type': 'application/json',
    },
    body: JSON.stringify(clientData),
  })
    .then(reponse => reponse.json())
    .catch(erreur => ({ erreur }));
}

function creerMoraleAPI(clientData) {
  console.log('Données envoyées', clientData);
  const FETCH_URL =
    'http://178.62.98.157:8080/digitall/api/clients/personne-morale/inscription';
  return fetch(FETCH_URL, {
    method: 'POST',
    headers: {
      accept: '*/*',
      'Content-Type': 'application/json',
    },
    body: JSON.stringify(clientData),
  })
    .then(reponse => ({ reponse: reponse.json() }))
    .catch(erreur => ({ erreur }));
}

function* moraleWorker(action) {
  const { reponse, erreur } = yield call(creerMoraleAPI, action.payload);
  console.log(reponse);
  if (erreur) yield put(actions.inscriptionEchouee(erreur.detail));
  else if (!reponse)
    yield put(
      actions.inscriptionEchouee(
        'Problème rencontré sur le serveur - Erreur 500',
      ),
    );
  else yield put(actions.inscriptionMoraleReussie(reponse));
}

function* physiqueWorker(action) {
  // const { reponse, erreur } = yield call(creerPhysiqueAPI, action.payload);
  const reponse = yield call(creerPhysiqueAPI, action.payload);
  console.log('Reponse', reponse);
  // console.log('Erreur', erreur);
  // if (erreur) yield put(actions.inscriptionEchouee(erreur.detail));
  if (!reponse)
    yield put(
      actions.inscriptionEchouee(
        'Problème rencontré sur le serveur - Erreur 500',
      ),
    );
  else yield put(actions.inscriptionPhysiqueReussie(reponse));
}

export function* signupSaga() {
  yield takeLatest(actions.inscriptionMoraleDemandee.type, moraleWorker);
  yield takeLatest(actions.inscriptionPhysiqueDemandee.type, physiqueWorker);
}
