import { Client } from 'types/Client';

/* --- STATE --- */
export interface SignupState {
  client: any;
  error: string;
  success: boolean;
  message: string;
  clientPhysique: any;
  clientMorale: any;
}
