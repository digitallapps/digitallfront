import { createSelector } from '@reduxjs/toolkit';

import { RootState } from 'types';
import { initialState } from '.';

const selectSlice = (state: RootState) => state.signup || initialState;

export const selectClient = createSelector(
  [selectSlice],
  state => state.client,
);
export const selectClientPhysique = createSelector(
  [selectSlice],
  state => state.clientPhysique,
);
export const selectClientMorale = createSelector(
  [selectSlice],
  state => state.clientMorale,
);
export const selectSuccess = createSelector(
  [selectSlice],
  state => state.success,
);
export const selectMessage = createSelector(
  [selectSlice],
  state => state.message,
);
export const selectErreur = createSelector([selectSlice], state => state.error);
