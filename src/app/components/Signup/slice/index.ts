import { PayloadAction } from '@reduxjs/toolkit';
import { createSlice } from 'utils/@reduxjs/toolkit';
import { useInjectReducer, useInjectSaga } from 'utils/redux-injectors';
import { signupSaga } from './saga';
import { SignupState } from './types';
import {
  Client,
  ClientPersonnePhysique,
  ClientPersonneMorale,
} from 'types/Client';

export const initialState: SignupState = {
  client: {},
  success: false,
  error: '',
  message: '',
  clientPhysique: {},
  clientMorale: {},
};

const slice = createSlice({
  name: 'signup',
  initialState,
  reducers: {
    inscriptionDemandee(state, action: PayloadAction<any>) {
      state.client = action.payload;
    },
    inscriptionEchouee(state, action: PayloadAction<any>) {
      state.error = action.payload;
    },
    inscriptionMoraleReussie(state, action: PayloadAction<any>) {
      state.clientMorale = action.payload;
    },
    inscriptionPhysiqueReussie(state, action: PayloadAction<any>) {
      state.clientPhysique = action.payload;
    },
    inscriptionPhysiqueDemandee: {
      reducer: (state, action: PayloadAction<any>) => {},
      prepare: (client, fonction, domaineActivite, adresse, telephone2) => {
        return {
          payload: {
            adresse,
            description: '',
            domaineActivite,
            ...client,
          },
        };
      },
    },
    inscriptionMoraleDemandee: {
      reducer: (state, action: PayloadAction<any>) => {},
      prepare: (client, capital, denomination, typeStructure, sigle) => {
        return {
          payload: {
            capital,
            denomination,
            sigle,
            typeStructure,
            ...client,
          },
        };
      },
    },
  },
});

export const { actions: signupActions } = slice;

export const useSignupSlice = () => {
  useInjectReducer({ key: slice.name, reducer: slice.reducer });
  useInjectSaga({ key: slice.name, saga: signupSaga });
  return { actions: slice.actions };
};
