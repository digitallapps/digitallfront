import React, { useState } from 'react';
import { Link, useHistory } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';
import { Formik, ErrorMessage } from 'formik';
import * as Yup from 'yup';
import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Card from 'react-bootstrap/Card';
import Form from 'react-bootstrap/Form';
import Button from 'react-bootstrap/Button';
import styled from 'styled-components/macro';
import { Statut, Visibilite, Client } from '../../../types/Client';
import { useSignupSlice } from './slice/index';
import { selectMessage, selectSuccess } from './slice/selectors';

export function Signup() {
  const dispatch = useDispatch();
  const { actions } = useSignupSlice();
  const [typeClient, setTypeClient] = useState('Personne Physique');
  const history = useHistory();
  const message = useSelector(selectMessage);
  const successLogin = useSelector(selectSuccess);

  const statuts = Object.keys(Statut).map(key => (
    <option key={key}>{Statut[key]}</option>
  ));
  const visibilites = Object.keys(Visibilite).map(key => (
    <option key={key}>{Visibilite[key]}</option>
  ));

  return (
    <Container>
      <CARD text="white" primary="true" body>
        <Formik
          initialValues={{
            prenom: '',
            nom: '',
            email: '',
            telephone1: '',
          }}
          validationSchema={Yup.object({
            prenom: Yup.string().required(),
            nom: Yup.string().required(),
            email: Yup.string()
              .email('Adresse e-mail invalide')
              .min(5, 'Adresse e-mail trop courte')
              .max(254, 'Adresse e-mail trop longue')
              .required('Adresse e-mail requise'),
          })}
          onSubmit={values => {
            dispatch(
              actions.inscriptionDemandee({
                prenom: values.prenom,
                nom: values.nom,
                email: values.email,
                telephone1: values.telephone1,
              }),
            );
            const redirectLink =
              typeClient === 'Personne Physique'
                ? '/signup/physique'
                : '/signup/morale';
            history.push(redirectLink);
          }}
        >
          {formik => (
            <Form onSubmit={formik.handleSubmit}>
              <Form.Group as={Row}>
                <Form.Label column="lg" md={3}>
                  Prénom
                </Form.Label>
                <Col md={9} lg={9}>
                  <FormControl
                    id="prenom"
                    type="text"
                    placeholder="Prénom"
                    {...formik.getFieldProps('prenom')}
                  />
                  <span style={{ color: '#F063B8' }}>
                    <ErrorMessage name="prenom" />
                  </span>
                </Col>
              </Form.Group>
              <Form.Group as={Row}>
                <Form.Label column="lg" md={3}>
                  Nom
                </Form.Label>
                <Col md={9} lg={9}>
                  <FormControl
                    id="nom"
                    type="text"
                    placeholder="Nom"
                    {...formik.getFieldProps('nom')}
                  />
                  <span style={{ color: '#F063B8' }}>
                    <ErrorMessage name="nom" />
                  </span>
                </Col>
              </Form.Group>
              <Form.Group as={Row}>
                <Form.Label column="lg" md={3}>
                  Email
                </Form.Label>
                <Col md={9} lg={9}>
                  <FormControl
                    id="email"
                    type="email"
                    placeholder="Adresse email"
                    {...formik.getFieldProps('email')}
                  />
                  <span style={{ color: '#F063B8' }}>
                    <ErrorMessage name="email" />
                  </span>
                </Col>
              </Form.Group>
              <Form.Group as={Row}>
                <Form.Label column="lg" md={3}>
                  N° Téléphone
                </Form.Label>
                <Col md={9} lg={9}>
                  <FormControl
                    id="telephone1"
                    type="number"
                    placeholder="Numéro de Téléphone"
                    {...formik.getFieldProps('telephone1')}
                  />
                  <span style={{ color: '#F063B8' }}>
                    <ErrorMessage name="telephone1" />
                  </span>
                </Col>
              </Form.Group>
              <Form.Group as={Row}>
                <Form.Label column="lg" md={3}>
                  Type de Client
                </Form.Label>
                <Col md={9} lg={9}>
                  <Form.Control
                    id="typeClient"
                    name="typeClient"
                    value={typeClient}
                    onChange={e => setTypeClient(e.target.value)}
                    as="select"
                    style={{ borderRadius: '25px' }}
                  >
                    <option>Personne Morale</option>
                    <option>Personne Physique</option>
                  </Form.Control>
                </Col>
              </Form.Group>
              <Row>
                <Col md={{ offset: 3 }}>
                  <BUTTON type="submit">S'inscrire</BUTTON>
                </Col>
              </Row>
              <Row>
                <Col md={{ offset: 3 }}>
                  <StyledLink to="/login">
                    {' '}
                    Vous avez déjà un compte? Connectez-vous{' '}
                  </StyledLink>
                </Col>
              </Row>
            </Form>
          )}
        </Formik>
      </CARD>
    </Container>
  );
}

const BUTTON = styled(Button)`
  border-radius: 50px;
`;

const CARD = styled(Card)`
  border-radius: 50px;
  padding: 25px;
  background: ${props => (props.primary ? '#212353' : 'white')};
`;

const FormControl = styled(Form.Control)`
  border-radius: 25px;
`;

const StyledLink = styled(Link)`
  color: ${p => p.theme.primary};
  text-decoration: none;

  &:hover {
    text-decoration: underline;
    opacity: 0.8;
  }

  &:active {
    opacity: 0.4;
  }
`;
