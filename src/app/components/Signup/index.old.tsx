import React, { useState } from 'react';
import { Link, Redirect } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';
import { Formik, ErrorMessage } from 'formik';
import * as Yup from 'yup';
import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Card from 'react-bootstrap/Card';
import Form from 'react-bootstrap/Form';
import Button from 'react-bootstrap/Button';
import styled from 'styled-components/macro';
import { Statut, Visibilite, Client } from '../../../types/Client';
import { useSignupSlice } from './slice/index';
import { selectClient } from './slice/selectors';

export function Signup() {
  const dispatch = useDispatch();
  const { actions } = useSignupSlice();
  const [typeClient, setTypeClient] = useState('Personne Physique');

  const statuts = Object.keys(Statut).map(key => (
    <option key={key} value={key}>
      {Statut[key]}
    </option>
  ));
  const visibilites = Object.keys(Visibilite).map(key => (
    <option key={key} value={key}>
      {Visibilite[key]}
    </option>
  ));

  const client = useSelector(selectClient);

  if (client.email)
    return (
      <CARD text="white" primary="true" body>
        <Card.Title className="text-center">
          Étape 2/3 Choix du type de Client
        </Card.Title>
        <br />
        <Row>
          <Col md={{ offset: 2 }}>
            <BUTTON href="/signup/physique" size="lg">
              Pesonne Physique
            </BUTTON>
          </Col>
          <Col>
            <BUTTON href="/signup/morale" size="lg" variant="secondary">
              Pesonne Morale
            </BUTTON>
          </Col>
        </Row>
      </CARD>
    );

  return (
    <Container>
      <CARD text="white" primary="true" body>
        <Card.Title className="text-center">
          Étape 1/3 - Informations de Base
        </Card.Title>
        <Formik
          initialValues={{
            email: '',
            password: '',
            confirmation: '',
            telephone1: '',
            visibilite: '',
            statut: '',
            domaineActivite: '',
          }}
          validationSchema={Yup.object({
            email: Yup.string()
              .email('Adresse e-mail invalide')
              .min(5, 'Adresse e-mail trop courte')
              .max(254, 'Adresse e-mail trop longue')
              .required('Adresse e-mail requise'),
            domaineActivite: Yup.string().max(
              50,
              "Domaine d'activité trop long",
            ),
          })}
          onSubmit={values => {
            dispatch(
              actions.inscriptionDemandee({
                email: values.email,
                statut: values.statut,
                visibilite: values.visibilite,
                domaineActivite: values.domaineActivite,
                telephone1: values.telephone1,
              } as Client),
            );
            // formActions.setSubmitting(false);
          }}
        >
          {formik => (
            <Form onSubmit={formik.handleSubmit}>
              <Form.Group as={Row}>
                <Form.Label column="lg" md={3}>
                  Email
                </Form.Label>
                <Col md={9} lg={9}>
                  <FormControl
                    id="email"
                    type="email"
                    placeholder="Adresse email"
                    {...formik.getFieldProps('email')}
                  />
                  <span style={{ color: '#F063B8' }}>
                    <ErrorMessage name="email" />
                  </span>
                </Col>
              </Form.Group>
              <Form.Group as={Row}>
                <Form.Label column="lg" md={3}>
                  Domaine d'activité
                </Form.Label>
                <Col md={9} lg={9}>
                  <FormControl
                    id="domaineActivite"
                    placeholder="Domaine d'activité"
                    {...formik.getFieldProps('domaineActivite')}
                  />
                </Col>
              </Form.Group>
              <Form.Group as={Row}>
                <Form.Label column="lg" md={3}>
                  N° Téléphone
                </Form.Label>
                <Col md={9} lg={9}>
                  <FormControl
                    id="telephone1"
                    type="number"
                    placeholder="Numéro de Téléphone"
                    {...formik.getFieldProps('telephone1')}
                  />
                  <span style={{ color: '#F063B8' }}>
                    <ErrorMessage name="telephone1" />
                  </span>
                </Col>
              </Form.Group>
              <Form.Group as={Row}>
                <Form.Label column="lg" md={3}>
                  Statut
                </Form.Label>
                <Col md={9} lg={9}>
                  <Form.Control
                    id="statut"
                    placeholder="Statut"
                    as="select"
                    {...formik.getFieldProps('statut')}
                  >
                    {statuts}
                  </Form.Control>
                  <span style={{ color: '#F063B8' }}>
                    <ErrorMessage name="statut" />
                  </span>
                </Col>
              </Form.Group>
              <Form.Group as={Row}>
                <Form.Label column="lg" md={3}>
                  Visibilité
                </Form.Label>
                <Col md={9} lg={9}>
                  <Form.Control
                    id="visibilite"
                    placeholder="Visibilité"
                    as="select"
                    {...formik.getFieldProps('visibilite')}
                  >
                    {visibilites}
                  </Form.Control>
                  <span style={{ color: '#F063B8' }}>
                    <ErrorMessage name="visibilite" />
                  </span>
                </Col>
              </Form.Group>
              <Form.Group as={Row}>
                <Form.Label column="lg" md={3}>
                  Type de Client
                </Form.Label>
                <Col md={9} lg={9}>
                  <Form.Control
                    id="typeClient"
                    name="typeClient"
                    value={typeClient}
                    onChange={e => setTypeClient(e.target.value)}
                    as="select"
                  >
                    <option>Personne Morale</option>
                    <option>Personne Physique</option>
                  </Form.Control>
                </Col>
              </Form.Group>
              <Row>
                <Col md={{ offset: 3 }}>
                  <BUTTON type="submit">S'inscrire</BUTTON>
                </Col>
              </Row>
              <Row>
                <Col md={{ offset: 3 }}>
                  <StyledLink to="/login">
                    {' '}
                    Vous avez déjà un compte? Connectez-vous{' '}
                  </StyledLink>
                </Col>
              </Row>
            </Form>
          )}
        </Formik>
      </CARD>
    </Container>
  );
}

const BUTTON = styled(Button)`
  border-radius: 50px;
`;

const CARD = styled(Card)`
  border-radius: 50px;
  padding: 25px;
  background: ${props => (props.primary ? '#212353' : 'white')};
`;

const FormControl = styled(Form.Control)`
  border-radius: 25px;
`;

const StyledLink = styled(Link)`
  color: ${p => p.theme.primary};
  text-decoration: none;

  &:hover {
    text-decoration: underline;
    opacity: 0.8;
  }

  &:active {
    opacity: 0.4;
  }
`;
