import React from 'react';
import { useLocation } from 'react-router-dom';
import { useSelector, useDispatch } from 'react-redux';
import { NavList, LinkStyled, Wrapper } from './Nav.styled';
import { selectToken, selectUser } from '../Login/slice/selectors';
import { useLoginSlice } from '../Login/slice/index';
import Navbar from 'react-bootstrap/Navbar';
import Button from 'react-bootstrap/Button';
import Modal from 'react-bootstrap/Modal';
import styled from 'styled-components/macro';
import logo from './assets/logo_blue.png';
import logodark from './assets/logo_white.png';
import { ThemeSwitch } from 'app/components/ThemeSwitch';
import { selectThemeKey } from 'styles/theme/slice/selectors';

const publicLinks = [
  { to: '/about', text: 'A propos' },
  { to: '/help', text: 'Aide' },
];

const guardedLinks = [
  { to: '/profil', text: 'Mon profil' },
  { to: '/offres', text: 'Offres' },
];

export function Nav() {
  const location = useLocation();
  const { actions } = useLoginSlice();
  const token = useSelector(selectToken);
  const theme = useSelector(selectThemeKey);

  const dispatch = useDispatch();

  const [modalShow, setModalShow] = React.useState(false);
  const handleClose = () => {
    setModalShow(false);
  };
  const handleShow = () => setModalShow(true);

  return (
    <Wrapper>
      <Navbar expand="lg">
        <Navbar.Brand href="/">
          {theme === 'light' ? (
            <img
              src={logo}
              height="40px"
              className="d-inline-block align-top"
              alt="Acceuil"
            />
          ) : (
            <img
              src={logodark}
              height="40px"
              className="d-inline-block align-top"
              alt="Acceuil"
            />
          )}
        </Navbar.Brand>
        <NavList className="my-auto">
          {token &&
            guardedLinks.map(item => (
              <li key={item.to}>
                <LinkStyled
                  to={item.to}
                  className={item.to === location.pathname ? 'active' : ''}
                >
                  {item.text}
                </LinkStyled>
              </li>
            ))}

          {publicLinks.map(item => (
            <li key={item.to}>
              <LinkStyled
                to={item.to}
                className={item.to === location.pathname ? 'active' : ''}
              >
                {item.text}
              </LinkStyled>
            </li>
          ))}

          {!token && (
            <li key="/login">
              <LinkStyled
                to="/login"
                className={'/login' === location.pathname ? 'active' : ''}
              >
                Se Connecter
              </LinkStyled>
            </li>
          )}
          {!token && (
            <li key="/signup">
              <LinkStyled
                to="/signup"
                className={'/signup' === location.pathname ? 'active' : ''}
              >
                S'inscrire
              </LinkStyled>
            </li>
          )}
          {token && (
            <>
              {/* <Navbar.Text>Connecté en tant que {user.firstName} {user.lastName}</Navbar.Text> */}
              <li key="/logout">
                <BUTTON variant="outline-primary" onClick={handleShow}>
                  Déconnexion
                </BUTTON>
              </li>
            </>
          )}
        </NavList>
        <ThemeSwitch />
      </Navbar>
      <Modal show={modalShow} onHide={handleClose} centered>
        <Modal.Header closeButton>
          <Modal.Title>Déconnexion</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          Vous êtes sur le point de vous déconnecter de votre compte.
        </Modal.Body>
        <Modal.Footer>
          <Button variant="secondary" onClick={handleClose}>
            Fermer
          </Button>
          <Button
            variant="danger"
            onClick={() => dispatch(actions.logoutRequested())}
          >
            Confirmer
          </Button>
        </Modal.Footer>
      </Modal>
    </Wrapper>
  );
}

const BUTTON = styled(Button)`
  border-radius: 50px;
  float: right;
`;
