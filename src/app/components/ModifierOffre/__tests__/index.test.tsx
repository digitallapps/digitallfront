import * as React from 'react';
import { render } from '@testing-library/react';

import { ModifierOffre } from '..';

jest.mock('react-i18next', () => ({
  useTranslation: () => {
    return {
      t: str => str,
      i18n: {
        changeLanguage: () => new Promise(() => {}),
      },
    };
  },
}));

describe('<ModifierOffre  />', () => {
  it('should match snapshot', () => {
    const loadingIndicator = render(<ModifierOffre />);
    expect(loadingIndicator.container.firstChild).toMatchSnapshot();
  });
});
