import { createSelector } from '@reduxjs/toolkit';

import { RootState } from 'types';
import { initialState } from '.';

const selectSlice = (state: RootState) => state.modifierOffre || initialState;

export const selectOffre = createSelector([selectSlice], state => state.offre);

export const selectErreur = createSelector(
  [selectSlice],
  state => state.erreur,
);

export const selectOffreModifiee = createSelector(
  [selectSlice],
  state => state.offreModifiee,
);
