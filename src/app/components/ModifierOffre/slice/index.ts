import { PayloadAction } from '@reduxjs/toolkit';
import { createSlice } from 'utils/@reduxjs/toolkit';
import { useInjectReducer, useInjectSaga } from 'utils/redux-injectors';
import { modifierOffreSaga } from './saga';
import { ModifierOffreState } from './types';

export const initialState: ModifierOffreState = {
  offre: {},
  offreModifiee: false,
  erreur: '',
};

const slice = createSlice({
  name: 'modifierOffre',
  initialState,
  reducers: {
    chargementOffreDemande(state, action: PayloadAction<any>) {},
    chargementOffreEffectue(state, action: PayloadAction<any>) {
      state.offre = action.payload;
    },
    chargementOffreEchoue(state, action: PayloadAction<string>) {
      state.erreur = action.payload;
    },
    modificationOffreDemandee(state, action: PayloadAction<any>) {},
    modificationOffreEffectuee(state, action: PayloadAction<any>) {
      state.offre = action.payload;
      state.offreModifiee = true;
    },
    modificationOffreEchouee(state, action: PayloadAction<string>) {
      state.erreur = action.payload;
    },
    nettoyerState(state) {
      state.offre = {};
      state.offreModifiee = false;
      state.erreur = '';
    },
  },
});

export const { actions: modifierOffreActions } = slice;

export const useModifierOffreSlice = () => {
  useInjectReducer({ key: slice.name, reducer: slice.reducer });
  useInjectSaga({ key: slice.name, saga: modifierOffreSaga });
  return { actions: slice.actions };
};
