/* --- STATE --- */
export interface ModifierOffreState {
  offre: any;
  offreModifiee: boolean;
  erreur: string;
}
