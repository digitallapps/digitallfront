import { call, put, takeLatest } from 'redux-saga/effects';
import { modifierOffreActions as actions } from '.';

function getOffreAPI(id, token) {
  const FETCH_URL = `http://178.62.98.157:8080/digitall/api/offres/${id}`;

  return fetch(FETCH_URL, {
    method: 'GET',
    headers: {
      accept: '*/*',
      'Content-Type': 'application/json',
      Authorization: `Bearer ${token}`,
    },
  }).then(reponse => reponse.json());
}

function modifierOffreAPI(id, token, nouvelleOffre) {
  const FETCH_URL = `http://178.62.98.157:8080/digitall/api/offres/modifier/${id}`;

  return fetch(FETCH_URL, {
    method: 'PUT',
    headers: {
      accept: '*/*',
      'Content-Type': 'application/json',
      Authorization: `Bearer ${token}`,
    },
    body: JSON.stringify(nouvelleOffre),
  }).then(reponse => reponse.json());
}

function* chargementOffreWorker(action) {
  const reponse = yield call(
    getOffreAPI,
    action.payload.id,
    action.payload.token,
  );
  if (reponse.libelle) yield put(actions.chargementOffreEffectue(reponse));
  else yield put(actions.chargementOffreEchoue(reponse.detail));
}

function* modificationOffreWorker(action) {
  const reponse = yield call(
    modifierOffreAPI,
    action.payload.id,
    action.payload.token,
    action.payload.nouvelleOffre,
  );
  console.log('Reponse', reponse);
  if (reponse.data) yield put(actions.modificationOffreEffectuee(reponse.data));
  else yield put(actions.modificationOffreEchouee(reponse.title));
}

export function* modifierOffreSaga() {
  yield takeLatest(actions.chargementOffreDemande.type, chargementOffreWorker);
  yield takeLatest(
    actions.modificationOffreDemandee.type,
    modificationOffreWorker,
  );
}
