/**
 *
 * ModifierOffre
 *
 */
import React, { useEffect } from 'react';
import styled from 'styled-components/macro';
import { useTranslation } from 'react-i18next';
import { useDispatch, useSelector } from 'react-redux';
import { useParams, useHistory } from 'react-router-dom';
import { ErrorMessage, Formik } from 'formik';
import * as Yup from 'yup';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Form from 'react-bootstrap/Form';
import Button from 'react-bootstrap/Button';
import Alert from 'react-bootstrap/Alert';
import Card from 'react-bootstrap/Card';
import { useModifierOffreSlice } from './slice/index';
import { selectToken } from '../Login/slice/selectors';
import {
  selectErreur,
  selectOffre,
  selectOffreModifiee,
} from './slice/selectors';

interface Params {
  id: string;
}

export function ModifierOffre(props) {
  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  const { t, i18n } = useTranslation();
  const dispatch = useDispatch();
  const params: Params = useParams();
  const history = useHistory();
  const { id } = params;
  const { actions } = useModifierOffreSlice();
  const token = useSelector(selectToken);
  const offre = useSelector(selectOffre);
  const erreur = useSelector(selectErreur);
  const offreModifiee = useSelector(selectOffreModifiee);

  useEffect(() => {
    dispatch(actions.chargementOffreDemande({ id, token }));
    return function nettoyer() {
      dispatch(actions.nettoyerState());
    };
  }, [params]);

  return (
    <div>
      {erreur && (
        <Alert variant="danger" className="text-center">
          {erreur}
        </Alert>
      )}
      <CARD text="white" primary="true">
        {offre.libelle && (
          <Formik
            initialValues={{
              libelle: offre.libelle,
              description: offre.description,
            }}
            validationSchema={Yup.object({
              libelle: Yup.string()
                .min(
                  10,
                  "Au minimum 10 charactères requis pour le libellé de l'offre",
                )
                .required('Libellé requis'),
              description: Yup.string()
                .min(30, 'Description trop courte')
                .required('Description requise'),
            })}
            onSubmit={(values, formActions) => {
              dispatch(
                actions.modificationOffreDemandee({
                  id: offre.id,
                  token,
                  nouvelleOffre: {
                    libelle: values.libelle,
                    description: values.description,
                  },
                }),
              );
              if (offreModifiee) history.push(`/offres/${offre.id}`);
            }}
          >
            {formik => (
              <Form onSubmit={formik.handleSubmit}>
                <Form.Group as={Row}>
                  <Form.Label column="lg" md={3}>
                    Libellé
                  </Form.Label>
                  <Col md={9} lg={9}>
                    <Form.Control
                      id="libelle"
                      placeholder="Libellé de l'offre"
                      {...formik.getFieldProps('libelle')}
                      type="text"
                    />
                    <span style={{ color: '#F063B8' }}>
                      <ErrorMessage name="libelle" />
                    </span>
                  </Col>
                </Form.Group>
                <Form.Group as={Row}>
                  <Form.Label column="lg" md={3}>
                    Description
                  </Form.Label>
                  <Col md={9} lg={9}>
                    <Form.Control
                      as="textarea"
                      id="description"
                      placeholder="Description complète de l'offre"
                      {...formik.getFieldProps('description')}
                    />
                    <span style={{ color: '#F063B8' }}>
                      <ErrorMessage name="description" />
                    </span>
                  </Col>
                </Form.Group>

                <Form.Group as={Row}>
                  <Col md={{ offset: 3 }}>
                    <BUTTON type="submit"> Enregistrer </BUTTON>
                  </Col>
                </Form.Group>
              </Form>
            )}
          </Formik>
        )}
      </CARD>
    </div>
  );
}

const BUTTON = styled(Button)`
  border-radius: 50px;
`;

const CARD = styled(Card)`
  padding: 25px;
  background: ${props => (props.primary ? '#212353' : 'white')};
`;
