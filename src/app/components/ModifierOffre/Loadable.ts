/**
 *
 * Asynchronously loads the component for ModifierOffre
 *
 */

import { lazyLoad } from 'utils/loadable';

export const ModifierOffre = lazyLoad(
  () => import('./index'),
  module => module.ModifierOffre,
);
