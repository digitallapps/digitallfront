import { createSelector } from '@reduxjs/toolkit';

import { RootState } from 'types';
import { initialState } from '.';

const selectSlice = (state: RootState) => state.creerOffre || initialState;

export const selectErreur = createSelector(
  [selectSlice],
  state => state.erreur,
);
