import { take, call, put, select, takeLatest } from 'redux-saga/effects';
import { creerOffreActions as actions } from '.';

function saisirOffreAPI(token, offreData) {
  const FETCH_URL = 'http://178.62.98.157:8080/digitall/api/offres/saisir';
  return fetch(FETCH_URL, {
    method: 'POST',
    headers: {
      accept: '*/*',
      'Content-Type': 'application/json',
      Authorization: `Bearer ${token}`,
    },
    body: JSON.stringify(offreData),
  })
    .then(reponse => reponse.json())
    .catch(erreur => ({ erreur }));
}

function* creerOffreWorker(action) {
  const reponse = yield call(
    saisirOffreAPI,
    action.payload.token,
    action.payload.offreData,
  );
  yield put(actions.offreCreeeAvecSucces(reponse.message));
}

export function* creerOffreSaga() {
  yield takeLatest(actions.creationOffreDemandee.type, creerOffreWorker);
}
