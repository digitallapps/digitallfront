import { PayloadAction } from '@reduxjs/toolkit';
import { createSlice } from 'utils/@reduxjs/toolkit';
import { useInjectReducer, useInjectSaga } from 'utils/redux-injectors';
import { creerOffreSaga } from './saga';
import { CreerOffreState } from './types';

export const initialState: CreerOffreState = {
  erreur: '',
};

const slice = createSlice({
  name: 'creerOffre',
  initialState,
  reducers: {
    creationOffreDemandee(state, action: PayloadAction<any>) {},
    offreCreeeAvecSucces(state, action: PayloadAction<string>) {
      state.erreur = '';
    },
  },
});

export const { actions: creerOffreActions } = slice;

export const useCreerOffreSlice = () => {
  useInjectReducer({ key: slice.name, reducer: slice.reducer });
  useInjectSaga({ key: slice.name, saga: creerOffreSaga });
  return { actions: slice.actions };
};
