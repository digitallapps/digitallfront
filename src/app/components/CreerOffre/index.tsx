/**
 *
 * CreerOffre
 *
 */
import * as React from 'react';
import styled from 'styled-components/macro';
import { useTranslation } from 'react-i18next';
import { useHistory } from 'react-router-dom';
import { ErrorMessage, Formik } from 'formik';
import * as Yup from 'yup';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Form from 'react-bootstrap/Form';
import Button from 'react-bootstrap/Button';
import Card from 'react-bootstrap/Card';
import { useDispatch, useSelector } from 'react-redux';
import { useCreerOffreSlice } from './slice/index';
import { selectToken } from '../Login/slice/selectors';
import { selectErreur } from './slice/selectors';

interface Props {}

export function CreerOffre(props: Props) {
  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  const { t, i18n } = useTranslation();
  const dispatch = useDispatch();
  const token = useSelector(selectToken);
  const erreur = useSelector(selectErreur);
  const { actions } = useCreerOffreSlice();
  const history = useHistory();

  return (
    <CARD text="white" primary="true" body>
      <Formik
        initialValues={{ libelle: '', description: '' }}
        validationSchema={Yup.object({
          libelle: Yup.string()
            .min(
              10,
              "Au minimum 10 charactères requis pour le libellé de l'offre",
            )
            .required('Libellé requis'),
          description: Yup.string()
            .min(30, 'Description trop courte')
            .required('Description requise'),
        })}
        onSubmit={(values, formActions) => {
          dispatch(
            actions.creationOffreDemandee({
              token,
              offreData: {
                libelle: values.libelle,
                description: values.description,
              },
            }),
          );
          if (!erreur) history.push('/offres');
        }}
      >
        {formik => (
          <Form onSubmit={formik.handleSubmit}>
            <Form.Group as={Row}>
              <Form.Label column="lg" md={3}>
                Libellé
              </Form.Label>
              <Col md={9} lg={9}>
                <Form.Control
                  id="libelle"
                  placeholder="Libellé de l'offre"
                  {...formik.getFieldProps('libelle')}
                  type="text"
                />
                <span style={{ color: '#F063B8' }}>
                  <ErrorMessage name="libelle" />
                </span>
              </Col>
            </Form.Group>

            <Form.Group as={Row}>
              <Form.Label column="lg" md={3}>
                Description
              </Form.Label>
              <Col md={9} lg={9}>
                <Form.Control
                  as="textarea"
                  id="description"
                  placeholder="Description complète de l'offre"
                  {...formik.getFieldProps('description')}
                />
                <span style={{ color: '#F063B8' }}>
                  <ErrorMessage name="description" />
                </span>
              </Col>
            </Form.Group>
            <Form.Group as={Row}>
              <Col md={{ offset: 3 }}>
                <BUTTON type="submit"> Créer l'offre </BUTTON>
              </Col>
            </Form.Group>
          </Form>
        )}
      </Formik>
    </CARD>
  );
}

const BUTTON = styled(Button)`
  border-radius: 50px;
`;

const CARD = styled(Card)`
  border-radius: 50px;
  padding: 25px;
  background: ${props => (props.primary ? '#212353' : 'white')};
`;
