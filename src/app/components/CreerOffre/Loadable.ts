/**
 *
 * Asynchronously loads the component for CreerOffre
 *
 */

import { lazyLoad } from 'utils/loadable';

export const CreerOffre = lazyLoad(
  () => import('./index'),
  module => module.CreerOffre,
);
