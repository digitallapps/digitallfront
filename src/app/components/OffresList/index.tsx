/**
 *
 * OffresList
 *
 */
import React, { useEffect } from 'react';
import { Link } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';
import Card from 'react-bootstrap/Card';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Badge from 'react-bootstrap/Badge';
import Button from 'react-bootstrap/Button';
import styled from 'styled-components/macro';
import { useOffresSlice } from './slice/index';
import { selectOffres } from './slice/selectors';
import { selectToken } from '../Login/slice/selectors';

interface Props {}

export function OffresList(props: Props) {
  const dispatch = useDispatch();
  const { actions } = useOffresSlice();
  const offres = useSelector(selectOffres);
  const token = useSelector(selectToken);

  useEffect(() => {
    dispatch(actions.offresRequested(token));
  }, []);
  // { console.log("Mes offres", offres) }
  return (
    <Div>
      <CARD primary="true" text="white" body>
        <Row>
          <Col>
            <Card.Title>Filtres</Card.Title>
          </Col>
          <Col>
            <Button variant="success">Validées</Button>
          </Col>
          <Col>
            <Button
              variant="warning"
              onClick={() => dispatch(actions.offresEnAttenteDemandees())}
            >
              En attente de validation
            </Button>
          </Col>
        </Row>
      </CARD>
      <br />
      {offres.map(offre => {
        return (
          <>
            <CARD key={offre.id}>
              <Card.Title>{offre.libelle}</Card.Title>
              <Card.Subtitle>
                <Badge
                  variant={
                    offre.statut === 'EN_ATTENTE_PUBLICATION'
                      ? 'warning'
                      : 'success'
                  }
                >
                  {offre.statut}
                </Badge>
              </Card.Subtitle>
              <Card.Body>
                <Card.Text className="text-justify">
                  {offre.description.substring(0, 300)}...
                </Card.Text>
                <Link to={`/offres/${offre.id}`}>En savoir plus</Link>
              </Card.Body>
              <Card.Footer>
                <StyledButton size="sm">
                  <StyledWhiteLink to={`/offres/${offre.id}/modifier`}>
                    Modifier
                  </StyledWhiteLink>
                </StyledButton>
                &nbsp;
                <StyledButton variant="secondary" size="sm">
                  <StyledWhiteLink to={`/offres/${offre.id}`}>
                    Compléter
                  </StyledWhiteLink>
                </StyledButton>
              </Card.Footer>
            </CARD>
            <br />
          </>
        );
      })}
    </Div>
  );
}

const Div = styled.div``;

const CARD = styled(Card)`
  padding: 25px;
  background: ${props => (props.primary ? '#212353' : 'white')};
`;

const StyledWhiteLink = styled(Link)`
  color: white;
  text-decoration: none;

  &:hover {
    text-decoration: underline;
    opacity: 0.8;
  }

  &:active {
    opacity: 0.4;
  }
`;
const StyledButton = styled(Button)`
  border-radius: 50px;
`;
