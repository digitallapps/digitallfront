import * as React from 'react';
import { render } from '@testing-library/react';

import { OffresList } from '..';

jest.mock('react-i18next', () => ({
  useTranslation: () => {
    return {
      t: str => str,
      i18n: {
        changeLanguage: () => new Promise(() => {}),
      },
    };
  },
}));

describe('<OffresList  />', () => {
  it('should match snapshot', () => {
    const loadingIndicator = render(<OffresList />);
    expect(loadingIndicator.container.firstChild).toMatchSnapshot();
  });
});
