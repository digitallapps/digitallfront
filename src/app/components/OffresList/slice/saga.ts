import {
  take,
  call,
  put,
  select,
  takeLatest,
  takeEvery,
} from 'redux-saga/effects';
import { offresActions as actions } from '.';

function getAllOffres(token) {
  const FETCH_URL = 'http://178.62.98.157:8080/digitall/api/offres/liste';

  return fetch(FETCH_URL, {
    method: 'GET',
    headers: {
      accept: 'application/json',
      Authorization: `Bearer ${token}`,
    },
  })
    .then(response => response.json())
    .catch(error => ({ error }));
}

function* offresWorker(action) {
  const response = yield call(getAllOffres, action.payload);
  yield put(actions.offresRetrieved(response));
}

export function* offresSaga() {
  yield takeLatest(actions.offresRequested.type, offresWorker);
}
