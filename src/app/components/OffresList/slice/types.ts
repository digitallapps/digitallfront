/* --- STATE --- */
export interface OffresState {
  items: Offre[];
}

export interface Offre {
  archiveePar: string;
  client: any;
  clotureePar: string;
  createdBy: string;
  createdDate: string;
  dateArchivage: string;
  dateCloture: string;
  datePublication: string;
  dateValidation: string;
  description: string;
  id: number;
  lastModifiedBy: string;
  lastModifiedDate: string;
  libelle: string;
  publicationValideePar: string;
  publieePar: string;
  statut: string;
  valideePar: string;
}
