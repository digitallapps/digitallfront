import { createSelector } from '@reduxjs/toolkit';

import { RootState } from 'types';
import { initialState } from '.';

const selectSlice = (state: RootState) => state.offres || initialState;

export const selectOffres = createSelector([selectSlice], state => state.items);
