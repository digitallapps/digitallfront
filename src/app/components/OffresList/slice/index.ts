import { PayloadAction } from '@reduxjs/toolkit';
import { createSlice } from 'utils/@reduxjs/toolkit';
import { useInjectReducer, useInjectSaga } from 'utils/redux-injectors';
import { offresSaga } from './saga';
import { Offre, OffresState } from './types';

export const initialState: OffresState = {
  items: [],
};

const slice = createSlice({
  name: 'offres',
  initialState,
  reducers: {
    offresRequested(state, action: PayloadAction<any>) {},
    offresRetrieved(state, action: PayloadAction<Offre[]>) {
      state.items = action.payload;
    },
    offresEnAttenteDemandees(state) {
      state.items = state.items.filter(
        item => item.statut === 'EN_ATTENTE_VALIDATION',
      );
    },
  },
});

export const { actions: offresActions } = slice;

export const useOffresSlice = () => {
  useInjectReducer({ key: slice.name, reducer: slice.reducer });
  useInjectSaga({ key: slice.name, saga: offresSaga });
  return { actions: slice.actions };
};

/**
 * Example Usage:
 *
 * export function MyComponentNeedingThisSlice() {
 *  const { actions } = useOffresSlice();
 *
 *  const onButtonClick = (evt) => {
 *    dispatch(actions.someAction());
 *   };
 * }
 */
