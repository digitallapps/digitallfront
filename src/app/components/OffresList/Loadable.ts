/**
 *
 * Asynchronously loads the component for OffresList
 *
 */

import { lazyLoad } from 'utils/loadable';

export const OffresList = lazyLoad(
  () => import('./index'),
  module => module.OffresList,
);
