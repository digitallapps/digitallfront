import * as React from 'react';
import { render } from '@testing-library/react';

import { FormInscriptionPersonneMorale } from '..';

jest.mock('react-i18next', () => ({
  useTranslation: () => {
    return {
      t: str => str,
      i18n: {
        changeLanguage: () => new Promise(() => {}),
      },
    };
  },
}));

describe('<FormInscriptionPersonneMorale  />', () => {
  it('should match snapshot', () => {
    const loadingIndicator = render(<FormInscriptionPersonneMorale />);
    expect(loadingIndicator.container.firstChild).toMatchSnapshot();
  });
});
