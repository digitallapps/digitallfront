/**
 *
 * FormInscriptionPersonneMorale
 *
 */
import * as React from 'react';
import styled from 'styled-components/macro';
import { useTranslation } from 'react-i18next';
import { messages } from './messages';
import { useDispatch, useSelector } from 'react-redux';
import { Formik, ErrorMessage } from 'formik';
import * as Yup from 'yup';
import Container from 'react-bootstrap/Container';
import Form from 'react-bootstrap/Form';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Card from 'react-bootstrap/Card';
import Alert from 'react-bootstrap/Alert';
import Button from 'react-bootstrap/Button';
import Modal from 'react-bootstrap/Modal';
import { Link } from 'react-router-dom';
import {
  selectClient,
  selectClientMorale,
  selectErreur,
} from '../Signup/slice/selectors';
import { useSignupSlice } from '../Signup/slice/index';

interface Props {}

export function FormInscriptionPersonneMorale(props: Props) {
  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  const { t, i18n } = useTranslation();
  const dispatch = useDispatch();
  const client = useSelector(selectClient);
  const clientMorale = useSelector(selectClientMorale);
  const erreur = useSelector(selectErreur);
  const { actions } = useSignupSlice();

  const [modalShow, setModalShow] = React.useState(false);
  const handleClose = () => {
    setModalShow(false);
  };
  const handleShow = () => setModalShow(true);

  return (
    <Container>
      {erreur && (
        <Alert variant="danger" className="text-center">
          {erreur}
        </Alert>
      )}
      <CARD text="white" primary="true" body>
        <Formik
          initialValues={{
            typeStructure: '',
            capital: 0,
            denomination: '',
            sigle: '',
          }}
          validationSchema={Yup.object({
            typeStructure: Yup.string().required('Type de structure requis'),
            capital: Yup.number(),
          })}
          onSubmit={(values, formActions) => {
            dispatch(
              actions.inscriptionMoraleDemandee(
                client,
                values.capital,
                values.denomination,
                values.typeStructure,
                values.sigle,
              ),
            );
            if (clientMorale && clientMorale.email) handleShow();
          }}
        >
          {formik => (
            <Form onSubmit={formik.handleSubmit}>
              <Form.Group as={Row}>
                <Form.Label column="lg" md={3}>
                  Capital
                </Form.Label>
                <Col md={9} lg={9}>
                  <FormControl
                    id="capital"
                    type="text"
                    placeholder="Capital"
                    {...formik.getFieldProps('capital')}
                  />
                </Col>
              </Form.Group>
              <Form.Group as={Row}>
                <Form.Label column="lg" md={3}>
                  Dénomination
                </Form.Label>
                <Col md={9} lg={9}>
                  <FormControl
                    id="denomination"
                    type="text"
                    placeholder="Dénomination"
                    {...formik.getFieldProps('denomination')}
                  />
                </Col>
              </Form.Group>
              <Form.Group as={Row}>
                <Form.Label column="lg" md={3}>
                  Type de Structure
                </Form.Label>
                <Col md={9} lg={9}>
                  <FormControl
                    id="typeStructure"
                    type="text"
                    placeholder="Type de Structure"
                    {...formik.getFieldProps('typeStructure')}
                  />
                </Col>
                <span style={{ color: '#F063B8' }}>
                  <ErrorMessage name="typeStructure" />
                </span>
              </Form.Group>
              <Form.Group as={Row}>
                <Form.Label column="lg" md={3}>
                  Sigle
                </Form.Label>
                <Col md={9} lg={9}>
                  <FormControl
                    id="Sigle"
                    type="text"
                    placeholder="Sigle"
                    {...formik.getFieldProps('sigle')}
                  />
                </Col>
              </Form.Group>
              <Row>
                <Col md={{ offset: 3 }}>
                  <BUTTON type="submit">Valider</BUTTON>
                </Col>
              </Row>
            </Form>
          )}
        </Formik>
      </CARD>

      <Modal show={modalShow} onHide={handleClose} centered>
        <Modal.Header closeButton>
          <Modal.Title>Statut Inscription</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <h3>Inscription effectuée avec succès!</h3>
          <StyledLink to="/login">
            Cliquez ici pour vous connecter avec votre nouveau compte.
          </StyledLink>
        </Modal.Body>
        <Modal.Footer>
          <Button variant="secondary" onClick={handleClose}>
            Fermer
          </Button>
        </Modal.Footer>
      </Modal>
    </Container>
  );
}

const Div = styled.div``;

const BUTTON = styled(Button)`
  border-radius: 50px;
`;

const CARD = styled(Card)`
  border-radius: 50px;
  padding: 25px;
  background: ${props => (props.primary ? '#212353' : 'white')};
`;

const FormControl = styled(Form.Control)`
  border-radius: 25px;
`;

const StyledLink = styled(Link)`
  color: ${p => p.theme.primary};
  text-decoration: none;

  &:hover {
    text-decoration: underline;
    opacity: 0.8;
  }

  &:active {
    opacity: 0.4;
  }
`;
