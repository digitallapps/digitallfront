/**
 *
 * Asynchronously loads the component for FormInscriptionPersonneMorale
 *
 */

import { lazyLoad } from 'utils/loadable';

export const FormInscriptionPersonneMorale = lazyLoad(
  () => import('./index'),
  module => module.FormInscriptionPersonneMorale,
);
