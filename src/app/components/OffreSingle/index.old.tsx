/**
 *
 * OffreSingle
 *
 */
import * as React from 'react';
import styled from 'styled-components/macro';
import { useTranslation } from 'react-i18next';
import { messages } from './messages';

import { LoadingIndicator } from '../LoadingIndicator';
import Card from 'react-bootstrap/Card';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import ListGroup from 'react-bootstrap/ListGroup';
import Table from 'react-bootstrap/Table';
import { Title } from '../../pages/OffreSinglePage/components/Title';

interface Props {}

export function OffreSingle(props: Props) {
  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  const { t, i18n } = useTranslation();

  {
    console.log(props);
  }

  return (
    <Div>
      {t('')}
      {/*  {t(...messages.someThing())}  */}
      <Title className="text-center">
        Requirements
        <LoadingIndicator />
      </Title>
      <Row>
        <Col>
          <Card>
            <Card.Body>
              <Card.Title>Unresolved Tickets</Card.Title>
              <Card.Text>
                <ListGroup variant="flush">
                  <ListGroup.Item>Ticket 1</ListGroup.Item>
                  <ListGroup.Item>Ticket 2</ListGroup.Item>
                  <ListGroup.Item>Ticket 3</ListGroup.Item>
                </ListGroup>
              </Card.Text>
            </Card.Body>
          </Card>
        </Col>
        <Col>
          <Card>
            <Card.Body>
              <Card.Title>Tasks</Card.Title>
              <Card.Text>
                <ListGroup variant="flush">
                  <ListGroup.Item>Tâche 1</ListGroup.Item>
                  <ListGroup.Item>Tâche 2</ListGroup.Item>
                  <ListGroup.Item>Tâche 3</ListGroup.Item>
                </ListGroup>
              </Card.Text>
            </Card.Body>
          </Card>
        </Col>
      </Row>
      <br />
      <Title className="text-center">
        Propositions
        <LoadingIndicator />
      </Title>
      <Card body>
        <Table borderless responsive hover>
          <thead>
            <tr>
              <th>Proposition details</th>
              <th>Freelancer Name</th>
              <th>Date</th>
              <th></th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td>Détail de la proposition</td>
              <td>Bachir Diop</td>
              <td>25/01/2021</td>
            </tr>
          </tbody>
        </Table>
      </Card>
    </Div>
  );
}

const Div = styled.div``;
