/**
 *
 * OffreSingle
 *
 */
import React, { useEffect, useState } from 'react';
import styled from 'styled-components/macro';
import { useDispatch, useSelector } from 'react-redux';
import { useParams } from 'react-router-dom';
import { ErrorMessage, Formik } from 'formik';
import * as Yup from 'yup';
import { LoadingIndicator } from '../LoadingIndicator';
import Card from 'react-bootstrap/Card';
import Alert from 'react-bootstrap/Alert';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Form from 'react-bootstrap/Form';
import ListGroup from 'react-bootstrap/ListGroup';
import Table from 'react-bootstrap/Table';
import Badge from 'react-bootstrap/Badge';
import Button from 'react-bootstrap/Button';
import Modal from 'react-bootstrap/Modal';
import { Title } from '../../pages/OffreSinglePage/components/Title';
import { useOffreSingleSlice } from './slice/index';
import { selectToken } from '../Login/slice/selectors';
import { Lead } from '../../pages/OffreSinglePage/components/Lead';
import {
  selectErreur,
  selectOffreSingle,
  selectOffreValidee,
  selectOffrePubliee,
} from './slice/selectors';

interface Props {}

interface Params {
  id: string;
}

export function OffreSingle(props: Props) {
  const params: Params = useParams();
  const dispatch = useDispatch();
  const token = useSelector(selectToken);
  const offre = useSelector(selectOffreSingle);
  const erreur = useSelector(selectErreur);
  const offreValidee = useSelector(selectOffreValidee);
  const offrePubliee = useSelector(selectOffrePubliee);
  const { actions } = useOffreSingleSlice();
  const { id } = params;

  const [show, setShow] = useState(false);
  const [showValiderModal, setShowValiderModal] = useState(false);
  const handleClose = () => setShow(false);
  const handleCloseValiderModal = () => setShowValiderModal(false);
  const handleShow = () => setShow(true);
  const handleShowValiderModal = () => setShowValiderModal(true);

  useEffect(() => {
    dispatch(actions.offreSingleRequested({ id, token }));
    return function nettoyer() {
      dispatch(actions.nettoyerState());
    };
  }, [id]);

  function handlePublier() {
    dispatch(actions.publicationOffreDemandee({ id: offre.id, token }));
    handleClose();
  }

  function handleValider() {
    dispatch(actions.validationOffreDemandee({ id: offre.id, token }));
    handleCloseValiderModal();
  }

  return (
    <Div>
      <Title className="text-center">{offre.libelle}</Title>
      <Lead className="text-center">
        Publiée le {new Date(offre.datePublication).toLocaleDateString()}
      </Lead>
      {erreur && (
        <Alert variant="danger" className="text-center">
          {erreur}
        </Alert>
      )}
      {offrePubliee && (
        <Alert variant="success" className="text-center">
          Publication effectuée avec succès!
        </Alert>
      )}
      {offreValidee && (
        <Alert variant="success" className="text-center">
          Validation effectuée avec succès!
        </Alert>
      )}
      <CARD primary="true">
        {offre.client && (
          <CARD body>
            <Card.Title>Client</Card.Title>
            <Card.Subtitle>
              {offre.client.user.firstName} {offre.client.user.lastName}
              <br />
              {offre.client.adresse || 'Adresse non renseignée'}
            </Card.Subtitle>
            <ListGroup variant="flush">
              <ListGroup.Item>
                <Row>
                  <Col>
                    <strong>Domaine d'activité:</strong>{' '}
                  </Col>
                  <Col>{offre.client.domaineActivite}</Col>
                </Row>
              </ListGroup.Item>
              <ListGroup.Item>
                <Row>
                  <Col>
                    <strong>Email:</strong>{' '}
                  </Col>
                  <Col>{offre.client.email}</Col>
                </Row>
              </ListGroup.Item>
              <ListGroup.Item>
                <strong>Description: </strong>
                <br />
                {offre.client.description || 'Aucune desciption disponible'}
              </ListGroup.Item>
            </ListGroup>
            <br />
            <h5>
              Description de l'offre
              <Badge
                variant={
                  offre.statut === 'EN_ATTENTE_PUBLICATION'
                    ? 'warning'
                    : 'success'
                }
              >
                {offre.statut}
              </Badge>
            </h5>
            <Card.Text>{offre.description}</Card.Text>
            <Card.Footer>
              <StyledButton
                variant="success"
                disabled={offre.valideePar}
                onClick={handleShowValiderModal}
              >
                Valider
              </StyledButton>
              <StyledButton
                variant="secondary"
                onClick={handleShow}
                disabled={offre.publieePar}
              >
                Publier
              </StyledButton>
            </Card.Footer>
          </CARD>
        )}
      </CARD>
      <Title className="text-center">
        Propositions
        <LoadingIndicator />
      </Title>
      <CARD primary="true" body>
        <Table className="text-light" borderless responsive hover>
          <thead>
            <tr>
              <th>Proposition details</th>
              <th>Freelancer Name</th>
              <th>Date</th>
              <th></th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td>Détail de la proposition</td>
              <td>Bachir Diop</td>
              <td>25/01/2021</td>
            </tr>
          </tbody>
        </Table>
      </CARD>
      <Modal show={show} onHide={handleClose}>
        <Modal.Header closeButton>
          <Modal.Title>Publier Offre</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          Vous êtes sur le point de publier cette offre, continuez?
        </Modal.Body>
        <Modal.Footer>
          <Button variant="secondary" onClick={handleClose}>
            Annuler
          </Button>
          <Button variant="primary" onClick={() => handlePublier()}>
            Publier
          </Button>
        </Modal.Footer>
      </Modal>
      <Modal show={showValiderModal} onHide={handleCloseValiderModal}>
        <Modal.Header closeButton>
          <Modal.Title>Valider Offre</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <Formik
            initialValues={{ datePublication: '' }}
            validationSchema={Yup.object({
              datePublication: Yup.string(),
            })}
            onSubmit={(values, formActions) => {
              formActions.setSubmitting(false);
            }}
          >
            {formik => (
              <Form onSubmit={formik.handleSubmit}>
                <Form.Group>
                  <Form.Label column="lg">Date de publication</Form.Label>
                  <Form.Control
                    id="datePublication"
                    type="date"
                    placeholder="Programmer la publication à cette date"
                    {...formik.getFieldProps('datePublication')}
                  />
                </Form.Group>
                <Button variant="info" onClick={() => handleValider()}>
                  Valider et Publier
                </Button>
              </Form>
            )}
          </Formik>
        </Modal.Body>
      </Modal>
    </Div>
  );
}

const Div = styled.div``;

const CARD = styled(Card)`
  padding: 25px;
  background: ${props => (props.primary ? '#212353' : 'white')};
`;

const StyledButton = styled(Button)`
  border-radius: 50px;
`;
