import { Offre } from '../../OffresList/slice/types';

/* --- STATE --- */
export interface OffreSingleState {
  data: Offre;
  offreValidee: boolean;
  offrePubliee: boolean;
  error: string;
}
