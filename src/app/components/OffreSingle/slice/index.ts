import { PayloadAction } from '@reduxjs/toolkit';
import { createSlice } from 'utils/@reduxjs/toolkit';
import { useInjectReducer, useInjectSaga } from 'utils/redux-injectors';
import { offreSingleSaga } from './saga';
import { OffreSingleState } from './types';
import { Offre } from '../../OffresList/slice/types';

export const initialState: OffreSingleState = {
  data: {} as Offre,
  offreValidee: false,
  offrePubliee: false,
  error: '',
};

const slice = createSlice({
  name: 'offreSingle',
  initialState,
  reducers: {
    offreSingleRequested(state, action: PayloadAction<any>) {},
    offreRetrievalSucceded(state, action: PayloadAction<Offre>) {
      state.data = action.payload;
    },
    offreRetrievalFailed(state, action: PayloadAction<string>) {
      state.error = action.payload;
    },
    validationOffreDemandee(state, action: PayloadAction<any>) {},
    validationOffreEffectuee(state, action: PayloadAction<Offre>) {
      state.data = action.payload;
      state.offreValidee = true;
    },
    validationOffreEchouee(state, action: PayloadAction<string>) {
      state.error = action.payload;
    },
    publicationOffreDemandee(state, action: PayloadAction<any>) {},
    publicationOffreEffectuee(state, action: PayloadAction<Offre>) {
      state.offrePubliee = true;
    },
    publicationOffreEchouee(state, action: PayloadAction<string>) {
      state.error = action.payload;
    },
    nettoyerState(state) {
      state.data = {} as Offre;
      state.offrePubliee = false;
      state.offreValidee = false;
      state.error = '';
    },
  },
});

export const { actions: offreSingleActions } = slice;

export const useOffreSingleSlice = () => {
  useInjectReducer({ key: slice.name, reducer: slice.reducer });
  useInjectSaga({ key: slice.name, saga: offreSingleSaga });
  return { actions: slice.actions };
};
