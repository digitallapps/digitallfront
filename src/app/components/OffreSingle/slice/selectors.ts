import { createSelector } from '@reduxjs/toolkit';

import { RootState } from 'types';
import { initialState } from '.';

const selectSlice = (state: RootState) => state.offreSingle || initialState;

export const selectOffreSingle = createSelector(
  [selectSlice],
  state => state.data,
);

export const selectErreur = createSelector([selectSlice], state => state.error);

export const selectOffreValidee = createSelector(
  [selectSlice],
  state => state.offreValidee,
);

export const selectOffrePubliee = createSelector(
  [selectSlice],
  state => state.offrePubliee,
);
