import { call, put, takeLatest } from 'redux-saga/effects';
import { offreSingleActions as actions } from '.';

function publierOffreAPI(id, token) {
  const FETCH_URL = `http://178.62.98.157:8080/digitall/api/offres/publier/${id}`;

  return fetch(FETCH_URL, {
    method: 'PUT',
    headers: {
      accept: '*/*',
      'Content-Type': 'application/json',
      Authorization: `Bearer ${token}`,
    },
  }).then(response => response.json());
}
function validerOffreAPI(id, token) {
  const FETCH_URL = `http://178.62.98.157:8080/digitall/api/offres/valider/${id}`;

  return fetch(FETCH_URL, {
    method: 'PUT',
    headers: {
      accept: '*/*',
      'Content-Type': 'application/json',
      Authorization: `Bearer ${token}`,
    },
  }).then(response => response.json());
}
function afficherOffreAPI(id, token) {
  const FETCH_URL = `http://178.62.98.157:8080/digitall/api/offres/afficher/${id}`;
  return fetch(FETCH_URL, {
    method: 'GET',
    headers: {
      accept: '*/*',
      'Content-Type': 'application/json',
      Authorization: `Bearer ${token}`,
    },
  }).then(response => response.json());
}
function* offreWorker(action) {
  const response = yield call(
    afficherOffreAPI,
    action.payload.id,
    action.payload.token,
  );
  console.log('Response', response);
  if (response) yield put(actions.offreRetrievalSucceded(response));
  else yield put(actions.offreRetrievalFailed('error.message'));
}
function* validationOffreWorker(action) {
  const reponse = yield call(
    validerOffreAPI,
    action.payload.id,
    action.payload.token,
  );
  if (reponse.data) {
    yield put(actions.validationOffreEffectuee(reponse.data));
    yield put(
      actions.offreSingleRequested({
        id: action.payload.id,
        token: action.payload.token,
      }),
    );
    // yield put(
    //   actions.publicationOffreDemandee({
    //     id: action.payload.id,
    //     token: action.payload.token,
    //   }),
    // );
  } else yield put(actions.validationOffreEchouee(reponse.title));
}
function* publicationOffreWorker(action) {
  const reponse = yield call(
    publierOffreAPI,
    action.payload.id,
    action.payload.token,
  );
  if (reponse.data) {
    yield put(actions.publicationOffreEffectuee(reponse.data));
    yield put(
      actions.offreSingleRequested({
        id: action.payload.id,
        token: action.payload.token,
      }),
    );
  } else yield put(actions.publicationOffreEchouee(reponse.title));
}

export function* offreSingleSaga() {
  yield takeLatest(actions.offreSingleRequested.type, offreWorker);
  yield takeLatest(actions.validationOffreDemandee.type, validationOffreWorker);
  yield takeLatest(
    actions.publicationOffreDemandee.type,
    publicationOffreWorker,
  );
}
