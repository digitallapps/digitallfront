/**
 *
 * Asynchronously loads the component for OffreSingle
 *
 */

import { lazyLoad } from 'utils/loadable';

export const OffreSingle = lazyLoad(
  () => import('./index'),
  module => module.OffreSingle,
);
