import * as React from 'react';
import { render } from '@testing-library/react';

import { FormInscriptionPersonnePhysique } from '..';

jest.mock('react-i18next', () => ({
  useTranslation: () => {
    return {
      t: str => str,
      i18n: {
        changeLanguage: () => new Promise(() => {}),
      },
    };
  },
}));

describe('<FormInscriptionPersonnePhysique  />', () => {
  it('should match snapshot', () => {
    const loadingIndicator = render(<FormInscriptionPersonnePhysique />);
    expect(loadingIndicator.container.firstChild).toMatchSnapshot();
  });
});
