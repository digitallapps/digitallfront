/**
 *
 * FormInscriptionPersonnePhysique
 *
 */
import * as React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useHistory } from 'react-router-dom';
import styled from 'styled-components/macro';
import { useTranslation } from 'react-i18next';
import { messages } from './messages';
import { Formik, ErrorMessage } from 'formik';
import * as Yup from 'yup';
import Alert from 'react-bootstrap/Alert';
import Form from 'react-bootstrap/Form';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Card from 'react-bootstrap/Card';
import Button from 'react-bootstrap/Button';
import ListGroup from 'react-bootstrap/ListGroup';
import Modal from 'react-bootstrap/Modal';
import { Link } from 'react-router-dom';
import {
  selectClient,
  selectClientPhysique,
  selectErreur,
} from '../Signup/slice/selectors';
import { useSignupSlice } from '../Signup/slice/index';

interface Props {}

export function FormInscriptionPersonnePhysique(props: Props) {
  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  const { t, i18n } = useTranslation();
  const dispatch = useDispatch();
  const client = useSelector(selectClient);
  const clientPhysique = useSelector(selectClientPhysique);
  const erreur = useSelector(selectErreur);
  const { actions } = useSignupSlice();
  const history = useHistory();

  const [modalShow, setModalShow] = React.useState(false);
  const handleClose = () => {
    setModalShow(false);
  };
  const handleShow = () => setModalShow(true);

  return (
    <Div>
      {t('')}
      {/*  {t(...messages.someThing())}  */}
      {erreur && (
        <Alert variant="danger" className="text-center">
          {erreur}
        </Alert>
      )}

      <CARD text="white" primary="true" body>
        <Formik
          initialValues={{
            domaineActivite: '',
            fonction: '',
            adresse: '',
            description: '',
            fax: '',
            telephone2: '',
          }}
          validationSchema={Yup.object({
            fonction: Yup.string().required(),
            domaineActivite: Yup.string().max(
              50,
              "Domaine d'activité trop long",
            ),
          })}
          onSubmit={(values, formActions) => {
            dispatch(
              actions.inscriptionPhysiqueDemandee(
                client,
                values.fonction,
                values.domaineActivite,
                values.adresse,
                values.telephone2,
              ),
            );
          }}
        >
          {formik => (
            <Form onSubmit={formik.handleSubmit}>
              <Form.Group as={Row}>
                <Form.Label column="lg" md={3}>
                  Domaine d'activité
                </Form.Label>
                <Col md={9} lg={9}>
                  <FormControl
                    id="domaineActivite"
                    placeholder="Domaine d'activité"
                    {...formik.getFieldProps('domaineActivite')}
                  />
                </Col>
              </Form.Group>
              <Form.Group as={Row}>
                <Form.Label column="lg" md={3}>
                  Fonction
                </Form.Label>
                <Col md={9} lg={9}>
                  <FormControl
                    id="fonction"
                    placeholder="Fonction"
                    {...formik.getFieldProps('fonction')}
                  />
                </Col>
              </Form.Group>
              <Form.Group as={Row}>
                <Form.Label column="lg" md={3}>
                  Adresse
                </Form.Label>
                <Col md={9} lg={9}>
                  <Form.Control
                    id="adresse"
                    placeholder="Adresse"
                    as="textarea"
                    {...formik.getFieldProps('adresse')}
                  />
                </Col>
              </Form.Group>
              <Form.Group as={Row}>
                <Form.Label column="lg" md={3}>
                  Autre N° Téléphone
                </Form.Label>
                <Col md={9} lg={9}>
                  <FormControl
                    id="telephone2"
                    type="number"
                    placeholder="Autre Numéro de Téléphone"
                    {...formik.getFieldProps('telephone2')}
                  />
                  <span style={{ color: '#F063B8' }}>
                    <ErrorMessage name="telephone2" />
                  </span>
                </Col>
              </Form.Group>
              <Row>
                <Col md={{ offset: 3 }}>
                  <BUTTON type="submit">S'inscrire</BUTTON>
                </Col>
              </Row>
              <Row>
                <Col md={{ offset: 3 }}>
                  <StyledLink to="/login">
                    {' '}
                    Vous avez déjà un compte? Connectez-vous{' '}
                  </StyledLink>
                </Col>
              </Row>
            </Form>
          )}
        </Formik>
      </CARD>

      <Modal show={modalShow} onHide={handleClose} centered>
        <Modal.Header closeButton>
          <Modal.Title>Statut Inscription</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <h3>Inscription effectuée avec succès!</h3>
          <StyledLink to="/login">
            Cliquez ici pour vous connecter avec votre nouveau compte.
          </StyledLink>
        </Modal.Body>
        <Modal.Footer>
          <Button variant="secondary" onClick={handleClose}>
            Fermer
          </Button>
        </Modal.Footer>
      </Modal>
    </Div>
  );
}

const Div = styled.div``;

const BUTTON = styled(Button)`
  border-radius: 50px;
`;

const CARD = styled(Card)`
  border-radius: 50px;
  padding: 25px;
  background: ${props => (props.primary ? '#212353' : 'white')};
`;

const FormControl = styled(Form.Control)`
  border-radius: 25px;
`;

const StyledLink = styled(Link)`
  color: ${p => p.theme.primary};
  text-decoration: none;

  &:hover {
    text-decoration: underline;
    opacity: 0.8;
  }

  &:active {
    opacity: 0.4;
  }
`;

const StyledListGroup = styled(ListGroup)`
  color: #212353;
`;
