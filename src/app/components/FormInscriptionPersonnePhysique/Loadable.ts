/**
 *
 * Asynchronously loads the component for FormInscriptionPersonnePhysique
 *
 */

import { lazyLoad } from 'utils/loadable';

export const FormInscriptionPersonnePhysique = lazyLoad(
  () => import('./index'),
  module => module.FormInscriptionPersonnePhysique,
);
