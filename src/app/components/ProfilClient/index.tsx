/**
 *
 * ProfilClient
 *
 */
import * as React from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { useLoginSlice } from '../Login/slice/index';
import { selectToken, selectUser } from '../Login/slice/selectors';
import { ErrorMessage, Formik } from 'formik';
import * as Yup from 'yup';
import Card from 'react-bootstrap/Card';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Form from 'react-bootstrap/Form';
import Button from 'react-bootstrap/Button';
import styled from 'styled-components/macro';

interface Props {}

export function ProfilClient(props: Props) {
  const dispatch = useDispatch();
  const token = useSelector(selectToken);
  const user = useSelector(selectUser);

  return (
    <>
      <CARD primary="true" text="white" className="text-center" body>
        <Card.Title>
          {user.firstName} {user.lastName}
        </Card.Title>
        <Card.Subtitle>{user.email}</Card.Subtitle>
        <Card.Text>{user.statut}</Card.Text>
      </CARD>
      <br />
      <CARD>
        <Card.Title>Modifier Profil</Card.Title>
        <Card.Body>
          <Card.Text>Informations Utilisateur</Card.Text>
          <Formik
            initialValues={{
              email: user.email,
              firstName: user.firstName,
              lastName: user.lastName,
              telephone1: '',
            }}
            validationSchema={Yup.object({
              email: Yup.string()
                .email('Adresse e-mail invalide')
                .required('Adresse e-mail requise'),
            })}
            onSubmit={(values, formActions) => {
              formActions.setSubmitting(false);
            }}
          >
            {formik => (
              <Form onSubmit={formik.handleSubmit}>
                <Form.Group>
                  <Form.Label>Email</Form.Label>
                  <FormControl
                    id="email"
                    type="email"
                    placeholder="Veuillez saisir votre adresse email"
                    {...formik.getFieldProps('email')}
                  />
                  <span style={{ color: '#F063B8' }}>
                    <ErrorMessage name="email" />
                  </span>
                </Form.Group>
                <Form.Group>
                  <Form.Label>Prénom</Form.Label>
                  <FormControl
                    id="firstName"
                    type="text"
                    placeholder="Veuillez saisir votre prénom"
                    {...formik.getFieldProps('firstName')}
                  />
                  <span style={{ color: '#F063B8' }}>
                    <ErrorMessage name="firstName" />
                  </span>
                </Form.Group>
                <Form.Group>
                  <Form.Label>Prénom</Form.Label>
                  <FormControl
                    id="lastName"
                    type="text"
                    placeholder="Veuillez saisir votre nom"
                    {...formik.getFieldProps('lastName')}
                  />
                  <span style={{ color: '#F063B8' }}>
                    <ErrorMessage name="lastName" />
                  </span>
                </Form.Group>
                <br />
                <hr />
                <br />
                <Form.Group>
                  <Form.Label>N° Téléphone</Form.Label>
                  <Form.Control
                    id="telephone1"
                    type="number"
                    placeholder="Numéro de Téléphone"
                    {...formik.getFieldProps('telephone1')}
                  />
                  <span style={{ color: '#F063B8' }}>
                    <ErrorMessage name="telephone1" />
                  </span>
                </Form.Group>
              </Form>
            )}
          </Formik>
        </Card.Body>
      </CARD>
    </>
  );
}

const CARD = styled(Card)`
  border-radius: 50px;
  padding: 25px;
  background: ${props => (props.primary ? '#212353' : 'white')};
`;

const FormControl = styled(Form.Control)`
  border-radius: 25px;
`;
