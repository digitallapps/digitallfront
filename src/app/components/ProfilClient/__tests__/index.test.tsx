import * as React from 'react';
import { render } from '@testing-library/react';

import { ProfilClient } from '..';

describe('<ProfilClient  />', () => {
  it('should match snapshot', () => {
    const loadingIndicator = render(<ProfilClient />);
    expect(loadingIndicator.container.firstChild).toMatchSnapshot();
  });
});
