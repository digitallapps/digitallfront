import { createSelector } from '@reduxjs/toolkit';

import { RootState } from 'types';
import { initialState } from '.';

const selectSlice = (state: RootState) => state.profilClient || initialState;

export const selectProfilClient = createSelector([selectSlice], state => state);
