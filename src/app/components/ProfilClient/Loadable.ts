/**
 *
 * Asynchronously loads the component for ProfilClient
 *
 */

import { lazyLoad } from 'utils/loadable';

export const ProfilClient = lazyLoad(
  () => import('./index'),
  module => module.ProfilClient,
);
