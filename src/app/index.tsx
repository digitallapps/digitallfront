/**
 *
 * App
 *
 * This component is the skeleton around the actual pages, and should only
 * contain code that should be seen on all pages. (e.g. navigation bar)
 */

import * as React from 'react';
import { Helmet } from 'react-helmet-async';
import { Switch, Route, BrowserRouter } from 'react-router-dom';

import { GlobalStyle } from 'styles/global-styles';

import { HomePage } from './pages/HomePage/Loadable';
import { LoginPage } from './pages/LoginPage/Loadable';
import { SignupPage } from './pages/SignupPage/Loadable';
import { ProfilClientPage } from './pages/ProfilClientPage/Loadable';
import { OffresListPage } from './pages/OffresListPage/Loadable';
import { OffreSinglePage } from './pages/OffreSinglePage/Loadable';
import { CreerOffrePage } from './pages/CreerOffrePage/Loadable';
import { ModifierOffrePage } from './pages/ModifierOffrePage/Loadable';
import { FormInscriptionPersonnePhysiquePage } from './pages/FormInscriptionPersonnePhysiquePage/Loadable';
import { FormInscriptionPersonneMoralePage } from './pages/FormInscriptionPersonneMoralePage/Loadable';
import { NotFoundPage } from './components/NotFoundPage/Loadable';
import { useTranslation } from 'react-i18next';
import { GuardedRoute } from '../utils/GuardedRoute';

export function App() {
  const { i18n } = useTranslation();
  return (
    <BrowserRouter>
      <Helmet
        titleTemplate="%s - DigitAll"
        defaultTitle="DigitAll"
        htmlAttributes={{ lang: i18n.language }}
      >
        <meta name="description" content="A Platform for top freelancers" />
      </Helmet>

      <Switch>
        <Route exact path={process.env.PUBLIC_URL + '/'} component={HomePage} />
        <Route exact path="/" component={HomePage} />
        <Route exact path="/login" component={LoginPage} />
        <Route
          exact
          path="/signup/physique"
          component={FormInscriptionPersonnePhysiquePage}
        />
        <Route
          exact
          path="/signup/morale"
          component={FormInscriptionPersonneMoralePage}
        />
        <Route exact path="/signup" component={SignupPage} />
        <GuardedRoute exact path="/profil" component={ProfilClientPage} />
        <Route exact path="/offres/creer" component={CreerOffrePage} />
        <GuardedRoute
          exact
          path="/offres/:id/modifier"
          component={ModifierOffrePage}
        />
        <GuardedRoute exact path="/offres/:id" component={OffreSinglePage} />
        <GuardedRoute exact path="/offres" component={OffresListPage} />
        <Route component={NotFoundPage} />
      </Switch>
      <GlobalStyle />
    </BrowserRouter>
  );
}
